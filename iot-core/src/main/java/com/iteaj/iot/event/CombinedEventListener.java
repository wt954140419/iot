package com.iteaj.iot.event;

import com.iteaj.iot.FrameworkComponent;
import com.iteaj.iot.client.IotClient;

public interface CombinedEventListener extends StatusEventListener<String, FrameworkComponent>{

    @Override
    default void onEvent(StatusEvent event) {
        String source;
        if(event.getSource() instanceof IotClient) {
            source = ((IotClient) event.getSource()).getConfig().connectKey();
        } else {
            source = (String) event.getSource();
        }

        switch (event.getStatus()) {
            case online:
                online(source, event.getComponent());
                break;
            case offline:
                offline(source, event.getComponent());
                break;

            default: throw new IllegalStateException("错误的设备事件类型: " + event.getStatus());
        }
    }

    @Override
    void online(String source, FrameworkComponent component);

    @Override
    void offline(String source, FrameworkComponent component);
}
