package com.iteaj.iot.test.message;

import com.iteaj.iot.ProtocolType;
import com.iteaj.iot.message.DefaultMessageHead;
import com.iteaj.iot.test.TestProtocolType;

public class TMessageHead extends DefaultMessageHead {

    public TMessageHead(byte[] message) {
        super(message);
    }

    public TMessageHead(String messageId, String equipCode, ProtocolType tradeType) {
        super(messageId, equipCode, tradeType);
    }

    @Override
    public TestProtocolType getType() {
        return (TestProtocolType) super.getType();
    }
}
