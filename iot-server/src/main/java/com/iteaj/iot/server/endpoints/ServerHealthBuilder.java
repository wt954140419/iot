package com.iteaj.iot.server.endpoints;

import com.iteaj.iot.ChannelManager;
import com.iteaj.iot.FrameworkManager;
import com.iteaj.iot.server.TcpServerComponent;
import com.iteaj.iot.server.udp.UdpServerComponent;
import com.sun.management.OperatingSystemMXBean;

import java.lang.management.ManagementFactory;
import java.lang.management.MemoryMXBean;
import java.lang.management.RuntimeMXBean;
import java.util.ArrayList;
import java.util.List;

public class ServerHealthBuilder {

    private static MemoryMXBean memoryMXBean = ManagementFactory.getMemoryMXBean();
    private static RuntimeMXBean mxb = ManagementFactory.getRuntimeMXBean();
    private static OperatingSystemMXBean system =
            (OperatingSystemMXBean) ManagementFactory.getOperatingSystemMXBean();

    public static ServerHealthEntity build() {
        ServerHealthEntity.JvmInfo jvmInfo = buildJvmInfo();
        ServerHealthEntity.SystemInfo systemInfo = buildSystemInfo();
        ServerHealthEntity.ComponentInfo componentInfo = buildComponentInfo();
        return new ServerHealthEntity(0, systemInfo, jvmInfo, componentInfo);
    }

    public static ServerHealthEntity.SystemInfo buildSystemInfo() {
        return new ServerHealthEntity.SystemInfo().setSystemCpuLoad(system.getSystemCpuLoad())
                .setOs(system.getName())
                .setArch(system.getArch())
                .setVersion(system.getVersion())
                .setProcessCpuLoad(system.getProcessCpuLoad())
                .setProcessCpuTime(system.getProcessCpuTime())
                .setFreePMSize(system.getFreePhysicalMemorySize())
                .setTotalPMSize(system.getTotalPhysicalMemorySize());
    }

    public static ServerHealthEntity.JvmInfo buildJvmInfo() {
        return new ServerHealthEntity.JvmInfo(memoryMXBean.getHeapMemoryUsage(), memoryMXBean.getNonHeapMemoryUsage());
    }

    public static ServerHealthEntity.ComponentInfo buildComponentInfo() {
        List<ServerHealthEntity.Component> components = new ArrayList<>();
        FrameworkManager.getInstance().getComponentFactory().servers().forEach(item -> {
            if(item instanceof TcpServerComponent) {
                ChannelManager deviceManager = (ChannelManager) item.getDeviceManager();
                components.add(new ServerHealthEntity.Component(((TcpServerComponent<?>) item).config().getPort(), item.getName()
                        , "tcp", item.startTime(), deviceManager.size(), deviceManager.useSize()));
            } else if(item instanceof UdpServerComponent) {
                components.add(new ServerHealthEntity.Component(((UdpServerComponent<?>) item).config().getPort(), item.getName(), "udp", item.startTime()));
            }
        });

        return new ServerHealthEntity.ComponentInfo(components);
    }

    public static Result toResult(long appStartTime) {
        ServerHealthEntity entity = ServerHealthBuilder.build();
        entity.setStartTime(appStartTime);
        return Result.success(entity);
    }

}
