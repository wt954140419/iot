package com.iteaj.iot.server.protocol;

import com.iteaj.iot.FreeProtocolHandle;

public interface ServerProtocolCallHandle extends FreeProtocolHandle<ServerInitiativeProtocol> {

    @Override
    default Object handle(ServerInitiativeProtocol protocol) {
        this.doHandle(protocol);
        return null;
    }

    void doHandle(ServerInitiativeProtocol protocol);
}
