package com.iteaj.iot.server.udp.impl;

import com.iteaj.iot.server.udp.UdpServerMessage;
import com.iteaj.iot.udp.UdpMessageBody;
import com.iteaj.iot.udp.UdpMessageHead;

import java.net.InetSocketAddress;

public class DefaultUdpServerMessage extends UdpServerMessage {

    protected DefaultUdpServerMessage(byte[] message) {
        super(message);
    }

    public DefaultUdpServerMessage(UdpMessageHead head) {
        super(head);
    }

    public DefaultUdpServerMessage(UdpMessageHead head, UdpMessageBody body) {
        super(head, body);
    }

    public DefaultUdpServerMessage(byte[] message, InetSocketAddress recipient) {
        super(message, recipient);
    }

    public DefaultUdpServerMessage(UdpMessageHead head, InetSocketAddress recipient) {
        super(head, recipient);
    }

    public DefaultUdpServerMessage(UdpMessageHead head, UdpMessageBody body, InetSocketAddress recipient) {
        super(head, body, recipient);
    }

    public DefaultUdpServerMessage(UdpMessageHead head, InetSocketAddress sender, InetSocketAddress recipient) {
        super(head, sender, recipient);
    }

    public DefaultUdpServerMessage(UdpMessageHead head, UdpMessageBody body, InetSocketAddress sender, InetSocketAddress recipient) {
        super(head, body, sender, recipient);
    }

    @Override
    protected UdpMessageHead doBuild(byte[] message) {
        String deviceSn = getSender().toString();
        deviceSn = deviceSn.startsWith("/") ? deviceSn.substring(1) : deviceSn;
        return new UdpMessageHead(deviceSn, message);
    }
}
