package com.iteaj.iot.client.http.impl;

import com.iteaj.iot.client.http.HttpClientMessage;
import com.iteaj.iot.client.http.HttpMethod;
import okhttp3.FormBody;
import okhttp3.RequestBody;

import java.util.HashMap;
import java.util.Map;

public class DefaultHttpClientMessage extends HttpClientMessage {

    protected DefaultHttpClientMessage(String url, HttpMethod method, Map<String, Object> queryParam) {
        super(url, method, queryParam);
    }

    public static DefaultHttpClientMessage get(String url) {
        return get(url, new HashMap<>());
    }

    public static DefaultHttpClientMessage get(String url, Map<String, Object> queryParam) {
        return new DefaultHttpClientMessage(url, HttpMethod.Get, queryParam);
    }

    public static DefaultHttpClientMessage post(String url) {
        return post(url, new HashMap<>());
    }

    public static DefaultHttpClientMessage post(String url, Map<String, Object> queryParam) {
        return post(url, queryParam, new HashMap<>());
    }

    public static DefaultHttpClientMessage post(String url, Map<String, Object> queryParam, Map<String, String> formBody) {
        FormBody.Builder builder = new FormBody.Builder();
        if(formBody != null) {
            formBody.forEach((key, value) -> {
                builder.add(key, value);
            });
        }

        return post(url, queryParam, builder.build());
    }

    public static DefaultHttpClientMessage post(String url, Map<String, Object> queryParam, RequestBody body) {
        return new DefaultHttpClientMessage(url, HttpMethod.Post, queryParam).setRequestBody(body);
    }

    @Override
    public DefaultHttpClientMessage setRequestBody(RequestBody requestBody) {
        return (DefaultHttpClientMessage) super.setRequestBody(requestBody);
    }
}
