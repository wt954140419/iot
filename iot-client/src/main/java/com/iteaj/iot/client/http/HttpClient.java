package com.iteaj.iot.client.http;

import com.iteaj.iot.client.ClientComponent;
import com.iteaj.iot.client.IotClient;

import java.util.function.Consumer;
import java.util.function.Function;

/**
 * http客户端
 */
public abstract class HttpClient implements IotClient {

    private HttpClientComponent component;
    private HttpClientConnectProperties properties;

    public HttpClient(HttpClientConnectProperties properties, HttpClientComponent component) {
        this.component = component;
        this.properties = properties;
    }

    @Override
    public int getPort() {
        return getConfig().getPort();
    }

    @Override
    public String getHost() {
        return getConfig().getHost();
    }

    @Override
    public HttpClientConnectProperties getConfig() {
        return this.properties;
    }

    public abstract void get(HttpClientMessage requestMessage);

    public abstract void get(HttpClientMessage requestMessage, Consumer<HttpClientMessage> call);

    public abstract void post(HttpClientMessage requestMessage);

    public abstract void post(HttpClientMessage requestMessage, Consumer<HttpClientMessage> call);

    @Override
    public Object connect() {
        return null;
    }

    @Override
    public Object disconnect() {
        return null;
    }

    @Override
    public ClientComponent getClientComponent() {
        return this.component;
    }
}
