package com.iteaj.iot.client.http;

import com.iteaj.iot.codec.filter.Interceptor;

public interface HttpInterceptor extends Interceptor<HttpClientComponent> {

}
