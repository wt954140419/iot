package com.iteaj.iot.client.http;

import com.iteaj.iot.FreeProtocolHandle;

public interface HttpProtocolHandler<T extends HttpClientProtocol> extends FreeProtocolHandle<T> {

    @Override
    default Object handle(T protocol) {
        this.doHandle(protocol);
        return null;
    }

    void doHandle(T protocol);
}
