package com.iteaj.iot.taos;

import com.iteaj.iot.Protocol;
import com.iteaj.iot.tools.annotation.TagsResolver;
import com.iteaj.iot.tools.db.IdType;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 用来声明超级表名, 可以自动创建数据表
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.TYPE })
public @interface STable {

    /**
     * 超级表表名
     * @return 返回超级表表名, 如果没有超级表请使用空字符串：""
     */
    String value() default "";

    /**
     * 数据表表名 使用${}取值
     * class TestEntity {
     *     private String sn;
     *     get set 省略
     * }
     * 数据表名=前缀D + 设备编号, 表达式 = "t_${sn}"
     * @see TaosHandle#handle(Protocol)
     * @return
     */
    String table();

    /**
     * 是否自动创建数据表
     * @return
     */
    boolean using() default false;
}
